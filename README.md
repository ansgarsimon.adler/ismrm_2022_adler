# ISMRM_2022_adler

Abstract for ISMRM 2022
The abstract text can be found here: https://pad.gwdg.de/Fx5JKm5AQ9Sd53sBbKB6_Q?both
The code is the last commit (d422a3da) at bart/Bloch_And_LBM_on_umg-devel .
In this git repo only the python scripts for imaging and the runscripts are presented. 
